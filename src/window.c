/* window.c
 *
 * Copyright 2024 Dialoginator Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "window.h"

#include <glib/gi18n.h>

struct _DialoginatorWindow
{
	AdwApplicationWindow  parent_instance;

	/* Template widgets */
	AdwHeaderBar        *header_bar;
	AdwToastOverlay     *toast_overlay;
	AdwSwitchRow        *modal_switch;
	AdwEntryRow         *heading_entry;
	AdwEntryRow         *body_entry;
	AdwExpanderRow      *button1_expand_row;
	AdwComboRow         *button1_appearance_comborow;
	AdwEntryRow         *button1_text_entry;
	AdwExpanderRow      *button2_expand_row;
	AdwComboRow         *button2_appearance_comborow;
  	AdwEntryRow         *button2_text_entry;
	AdwExpanderRow      *button3_expand_row;
	AdwComboRow         *button3_appearance_comborow;
	AdwEntryRow         *button3_text_entry;
	AdwExpanderRow      *button4_expand_row;
	AdwComboRow         *button4_appearance_comborow;
	AdwEntryRow         *button4_text_entry;
	GtkButton           *showbutton;
};

G_DEFINE_FINAL_TYPE (DialoginatorWindow, dialoginator_window, ADW_TYPE_APPLICATION_WINDOW)

static void
dialoginator_window_class_init (DialoginatorWindowClass *klass)
{
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	gtk_widget_class_set_template_from_resource (widget_class, "/page/codeberg/sungsphinx/Dialoginator/window.ui");
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, header_bar);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, toast_overlay);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, modal_switch);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, heading_entry);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, body_entry);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button1_expand_row);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button1_appearance_comborow);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button1_text_entry);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button2_expand_row);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button2_appearance_comborow);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button2_text_entry);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button3_expand_row);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button3_appearance_comborow);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button3_text_entry);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button4_expand_row);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button4_appearance_comborow);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, button4_text_entry);
	gtk_widget_class_bind_template_child (widget_class, DialoginatorWindow, showbutton);
}

static void
show_toast (DialoginatorWindow *self,
            const char *heading,
            const char *button_label,
            const char *action_name,
            AdwToastOverlay *overlay)
{
  if (overlay == NULL) {
    g_error ("show_toast: no AdwToastOverlay was passed, or the pointer was NULL!");
  };

  AdwToast *toast = adw_toast_new (heading);

  if (button_label == NULL || action_name == NULL) {
    g_debug ("show_toast: button label and/or action was not passed, not adding a button to toast.");
  } else {
    adw_toast_set_button_label (toast, button_label);
    adw_toast_set_action_name (toast, action_name);
  }

  adw_toast_overlay_add_toast (ADW_TOAST_OVERLAY (overlay), toast);
}

/* static void */
/* toast_test (GSimpleAction *action, */
/*             GVariant      *parameter, */
/*             gpointer       user_data) */
/* { */
/*   DialoginatorWindow *self = user_data; */
/*   show_toast (self, "Oops, something went wrong!", "Details", NULL, self->toast_overlay); */
/* } */

static void
show_dialog (GtkButton *showbutton,
             DialoginatorWindow *self)
{
  AdwDialog *dialog;
  const char *heading;
  const char *body;

  heading = gtk_editable_get_text (GTK_EDITABLE (self->heading_entry));
  body = gtk_editable_get_text (GTK_EDITABLE (self->body_entry));

  dialog = adw_alert_dialog_new (heading, body);

  if (adw_expander_row_get_enable_expansion (self->button1_expand_row) == true) {
    adw_alert_dialog_add_response (ADW_ALERT_DIALOG (dialog), "button1", gtk_editable_get_text (GTK_EDITABLE (self->button1_text_entry)));
    if (adw_combo_row_get_selected (self->button1_appearance_comborow) == 0) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button1", ADW_RESPONSE_DEFAULT);
    } else if (adw_combo_row_get_selected (self->button1_appearance_comborow) == 1) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button1", ADW_RESPONSE_SUGGESTED);
    } else if (adw_combo_row_get_selected (self->button1_appearance_comborow) == 2) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button1", ADW_RESPONSE_DESTRUCTIVE);
    }
  }

  if (adw_expander_row_get_enable_expansion (self->button2_expand_row) == true) {
    adw_alert_dialog_add_response (ADW_ALERT_DIALOG (dialog), "button2", gtk_editable_get_text (GTK_EDITABLE (self->button2_text_entry)));
    if (adw_combo_row_get_selected (self->button2_appearance_comborow) == 0) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button2", ADW_RESPONSE_DEFAULT);
    } else if (adw_combo_row_get_selected (self->button2_appearance_comborow) == 1) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button2", ADW_RESPONSE_SUGGESTED);
    } else if (adw_combo_row_get_selected (self->button2_appearance_comborow) == 2) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button2", ADW_RESPONSE_DESTRUCTIVE);
    }
  }

  if (adw_expander_row_get_enable_expansion (self->button3_expand_row) == true) {
    adw_alert_dialog_add_response (ADW_ALERT_DIALOG (dialog), "button3", gtk_editable_get_text (GTK_EDITABLE (self->button3_text_entry)));
    if (adw_combo_row_get_selected (self->button3_appearance_comborow) == 0) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button3", ADW_RESPONSE_DEFAULT);
    } else if (adw_combo_row_get_selected (self->button3_appearance_comborow) == 1) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button3", ADW_RESPONSE_SUGGESTED);
    } else if (adw_combo_row_get_selected (self->button3_appearance_comborow) == 2) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button3", ADW_RESPONSE_DESTRUCTIVE);
    }
  }

  if (adw_expander_row_get_enable_expansion (self->button4_expand_row) == true) {
    adw_alert_dialog_add_response (ADW_ALERT_DIALOG (dialog), "button4", gtk_editable_get_text (GTK_EDITABLE (self->button4_text_entry)));
    if (adw_combo_row_get_selected (self->button4_appearance_comborow) == 0) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button4", ADW_RESPONSE_DEFAULT);
    } else if (adw_combo_row_get_selected (self->button4_appearance_comborow) == 1) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button4", ADW_RESPONSE_SUGGESTED);
    } else if (adw_combo_row_get_selected (self->button4_appearance_comborow) == 2) {
      adw_alert_dialog_set_response_appearance (ADW_ALERT_DIALOG (dialog), "button4", ADW_RESPONSE_DESTRUCTIVE);
    }
  }

  if (adw_switch_row_get_active (self->modal_switch) == false) {
    adw_dialog_present (dialog, NULL);
  } else {
    adw_dialog_present (dialog, GTK_WIDGET (self));
  }
}

/* static const GActionEntry window_actions[] = { */
/* 	{ "toast", toast_test }, */
/* }; */

static void
dialoginator_window_init (DialoginatorWindow *self)
{
	/* g_action_map_add_action_entries (G_ACTION_MAP (self), */
	/*                                  window_actions, */
	/*                                  G_N_ELEMENTS (window_actions), */
	/*                                  self); */
	gtk_widget_init_template (GTK_WIDGET (self));
	if (g_strcmp0 (PROFILE, ".Devel") == 0)
	gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
	g_signal_connect (self->showbutton, "clicked", G_CALLBACK (show_dialog), self);
}
