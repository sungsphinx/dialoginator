[![Please do not theme this app](https://stopthemingmy.app/badge.svg)](https://stopthemingmy.app)

<img style="vertical-align: middle;" src="data/icons/hicolor/scalable/apps/page.codeberg.sungsphinx.Dialoginator.svg" width="120" height="120" align="left">

# Dialoginator (should probably have a better name)
Dialog meme maker

(made because of [sonny on Mastodon](https://floss.social/@sonny/112235179782834647))
</div>
<br/>

## Code of Conduct
Please follow the [GNOME Code of Conduct](https://conduct.gnome.org/) when interacting with this project, thank you :D

## Install

### Install (Development Version)

#### You can either:
Use [GNOME Builder](https://apps.gnome.org/Builder) and build the application with a breeze.

#### Or you can build it manually:

**1.** Install Flatpak (if it isn't installed already, **with [Flathub](https://flathub.org) and [GNOME Nightly](https://wiki.gnome.org/Apps/Nightly)**) and Flatpak Builder.

**2.** Make sure you have ```runtime/org.gnome.Platform/[x86_64/aarch64]/master``` &amp; ```runtime/org.gnome.Sdk/[x86_64/aarch64]/master``` installed, you can install the platform with:

```
flatpak install runtime/org.gnome.Platform/$(uname -m)/master
```

And the SDK with:

```
flatpak install runtime/org.gnome.Platform/$(uname -m)/master
```
### (Choose x86_64 for 64/32bit, aarch64 for arm64)

**3.** Download the [Flatpak Manifest](build-aux/flatpak/page.codeberg.sungsphinx.Dialoginator.Devel.json) and optionally make a folder to put it in.

**4.** Open a terminal in the folder (if you created one) and choose one of the following to run:

* Install Directly: 
```
flatpak run org.flatpak.Builder --install --force-clean build-dir page.codeberg.sungsphinx.Dialoginator.Devel.json
```

* Build Bundle: 

```
flatpak run org.flatpak.Builder --repo=repo --force-clean build-dir page.codeberg.sungsphinx.Dialoginator.Devel.json
```

Then run:

```
flatpak build-bundle repo page.codeberg.sungsphinx.Dialoginator.Devel.flatpak page.codeberg.sungsphinx.Dialoginator.Devel
``` 
This will create a bundle (*.flatpak). Read more about single-file bundles [here](https://docs.flatpak.org/en/latest/single-file-bundles.html).
