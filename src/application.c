/* application.c
 *
 * Copyright 2024 Dialoginator Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <glib/gi18n.h>

#include "config.h"

#include "application.h"
#include "window.h"

struct _DialoginatorApplication
{
	AdwApplication parent_instance;
};

G_DEFINE_FINAL_TYPE (DialoginatorApplication, dialoginator_application, ADW_TYPE_APPLICATION)

DialoginatorApplication *
dialoginator_application_new (const char        *application_id,
                              GApplicationFlags  flags)
{
	g_return_val_if_fail (application_id != NULL, NULL);

	return g_object_new (DIALOGINATOR_TYPE_APPLICATION,
	                     "application-id", application_id,
	                     "flags", flags,
                             "resource-base-path", "/page/codeberg/sungsphinx/Dialoginator",
	                     NULL);
}

static void
dialoginator_application_activate (GApplication *app)
{
	GtkWindow *window;

	g_assert (DIALOGINATOR_IS_APPLICATION (app));

	window = gtk_application_get_active_window (GTK_APPLICATION (app));

	if (window == NULL)
		window = g_object_new (DIALOGINATOR_TYPE_WINDOW,
		                       "application", app,
		                       NULL);

	gtk_window_present (window);
}

static void
dialoginator_application_class_init (DialoginatorApplicationClass *klass)
{
	GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

	app_class->activate = dialoginator_application_activate;
}

static void
dialoginator_application_about_action (GSimpleAction *action,
                                       GVariant      *parameter,
                                       gpointer       user_data)
{
      static const char *developers[] = {"Dexter Reed https://sungsphinx.codeberg.page", NULL};
      const char *copyright = "© 2024 Dialoginator Contributors";


      DialoginatorApplication *self = user_data;
      GtkWindow *window = gtk_application_get_active_window (GTK_APPLICATION (self));
      AdwDialog *dialog;


      g_assert (DIALOGINATOR_IS_APPLICATION (self));

      window = gtk_application_get_active_window (GTK_APPLICATION (self));

      dialog = adw_about_dialog_new_from_appdata ("/page/codeberg/sungsphinx/Dialoginator/dialoginator.metainfo.xml", VERSION_NO_SUFFIX);

      adw_about_dialog_set_version (ADW_ABOUT_DIALOG (dialog), VERSION);

      adw_about_dialog_set_comments (ADW_ABOUT_DIALOG (dialog), _("Dialog meme maker"));

      /* adw_about_dialog_add_link (ADW_ABOUT_DIALOG (dialog), */
      /*                                   _("_Application Source Code"), */
      /*                             "https://gitlab.gnome.org/sungsphinx/dialoginator"); */

      adw_about_dialog_set_copyright (ADW_ABOUT_DIALOG (dialog), copyright);

      adw_about_dialog_set_developers (ADW_ABOUT_DIALOG (dialog), developers);

      adw_dialog_present (dialog, GTK_WIDGET (window));
}

static void
dialoginator_application_quit_action (GSimpleAction *action,
                                      GVariant      *parameter,
                                      gpointer       user_data)
{
	DialoginatorApplication *self = user_data;

	g_assert (DIALOGINATOR_IS_APPLICATION (self));

	g_application_quit (G_APPLICATION (self));
}

static const GActionEntry app_actions[] = {
	{ "quit", dialoginator_application_quit_action },
	{ "about", dialoginator_application_about_action },
};

static void
dialoginator_application_init (DialoginatorApplication *self)
{
	g_action_map_add_action_entries (G_ACTION_MAP (self),
	                                 app_actions,
	                                 G_N_ELEMENTS (app_actions),
	                                 self);
	gtk_application_set_accels_for_action (GTK_APPLICATION (self),
	                                       "app.quit",
	                                       (const char *[]) { "<primary>q", NULL });
}
